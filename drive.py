# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################


import FreeCAD

class Drive:
    def __init__(self, obj, label): 
        
        obj.addExtension("App::GroupExtensionPython")
        
        obj.addProperty("App::PropertyString","label","Drive","label",1).label = label
        
        #function type
        obj.addProperty("App::PropertyEnumeration","type","Drive","type")
        obj.type=['const, <const_coef>',
                  'cosine, <initial_time>, <angular_vel>, <amplitude>, half, <initial_value>',
                  'cosine, <initial_time>, <angular_vel>, <amplitude>, one, <initial_value>',
                  'cosine, <initial_time>, <angular_vel>, <amplitude>, forever, <initial_value>',
                  'cosine, <initial_time>, <angular_vel>, <amplitude>, <number_of_cycles>, <initial_value>',                              
                  'cubic, <const_coef>, <linear_coef>, <parabolic_coef>, <cubic_coef>',
                  'direct',
                  'double ramp, <a_slope>, <a_initial_time>, <a_final_time>, <d_slope>, <d_initial_time>, forever, <initial_value>',
                  'double ramp, <a_slope>, <a_initial_time>, <a_final_time>, <d_slope>, <d_initial_time>, <d_final_time>, <initial_value>',
                  'double step, <initial_time>, <final_time>, <step_value>, <initial_value>',
                  'linear, <const_coef>, <slope_coef>',
                  'ramp, <slope>, <initial_time>, forever, <initial_value>',
                  'ramp, <slope>, <initial_time>, <final_time>, <initial_value>',
                  'sine, <initial_time>, <angular_vel>, <amplitude>, half, <initial_value>',
                  'sine, <initial_time>, <angular_vel>, <amplitude>, one, <initial_value>',
                  'sine, <initial_time>, <angular_vel>, <amplitude>, forever, <initial_value>',
                  'sine, <initial_time>, <angular_vel>, <amplitude>, <number_of_cycles>, <initial_value>',
                  'step, <initial_time>, <step_value>, <initial_value>',
                  'string',
                  'time',
                  'timestep',
                  'unit']
                  

        #Slope parameters:
        obj.addProperty("App::PropertyFloat","a_slope","Slope parameters","a_slope").a_slope = 2.0
        obj.addProperty("App::PropertyFloat","slope","Slope parameters","slope").slope = 0.1
        obj.addProperty("App::PropertyFloat","slope_coef","Slope parameters","slope_coef").slope_coef = 0.1
        obj.addProperty("App::PropertyFloat","d_slope","Slope parameters","d_slope").d_slope = -2.0               
                
        #coefficient parameters
        obj.addProperty("App::PropertyFloat","const_coef","Coefficient parameters","const_coef").const_coef = 10.0
        obj.addProperty("App::PropertyFloat","linear_coef","Coefficient parameters","linear_coef").linear_coef = 1.0
        obj.addProperty("App::PropertyFloat","parabolic_coef","Coefficient parameters","parabolic_coef").parabolic_coef = 1.0
        obj.addProperty("App::PropertyFloat","cubic_coef","Coefficient parameters","cubic_coef").cubic_coef = 1.0 
        
        #Drive parameters
        obj.addProperty("App::PropertyFloat","step_value","Drive parameters","step_value").step_value = 10.0        
        obj.addProperty("App::PropertyFloat","angular_vel","Drive parameters","angular_vel").angular_vel = 3.11      
        obj.addProperty("App::PropertyFloat","amplitude","Drive parameters","amplitude").amplitude = 10.0
        obj.addProperty("App::PropertyFloat","initial_value","Drive parameters","initial_value").initial_value = 0.0
        obj.addProperty("App::PropertyInteger","number_of_cycles","Drive parameters","number_of_cycles").number_of_cycles = 2
        
        #Time parameters:
        obj.addProperty("App::PropertyQuantity","a_initial_time","Time parameters","a_initial_time").a_initial_time = 1.0
        obj.a_initial_time = FreeCAD.Units.Unit('s') 
        
        obj.addProperty("App::PropertyQuantity","d_initial_time","Time parameters","d_initial_time").d_initial_time = 4.0
        obj.d_initial_time = FreeCAD.Units.Unit('s')
        
        obj.addProperty("App::PropertyQuantity","a_final_time","Time parameters","a_final_time").a_final_time = 2.0
        obj.a_final_time = FreeCAD.Units.Unit('s')
        
        obj.addProperty("App::PropertyQuantity","d_final_time","Time parameters","d_final_time").d_final_time = 5.0
        obj.d_final_time = FreeCAD.Units.Unit('s')
        
        obj.addProperty("App::PropertyQuantity","initial_time","Time parameters","initial_time").initial_time = 2.0
        obj.initial_time = FreeCAD.Units.Unit('s')
        
        obj.addProperty("App::PropertyQuantity","final_time","Time parameters","final_time").final_time = 4.0
        obj.final_time = FreeCAD.Units.Unit('s')
        
        
        obj.addProperty("App::PropertyString","expression","Drive","expression",1).expression = '1.5*sin(2.*Time)'
        
        obj.Proxy = self      
                               
        FreeCAD.ActiveDocument.recompute()

    def onChanged(self, fp, prop):
        try:
            if(fp.type == 'const, <const_coef>'):
                fp.expression = 'const, ' + str(fp.const_coef)
                            
            if(fp.type == 'cosine, <initial_time>, <angular_vel>, <amplitude>, half, <initial_value>'):
                fp.expression = 'cosine, ' + str(fp.initial_time.Value) + ', ' + str(fp.angular_vel) + ', ' + str(fp.amplitude) + ', half, ' + str(fp.initial_value)
    
            if(fp.type == 'cosine, <initial_time>, <angular_vel>, <amplitude>, one, <initial_value>'):
                fp.expression = 'cosine, ' + str(fp.initial_time.Value) + ', ' + str(fp.angular_vel) +', ' + str(fp.amplitude) + ', one, ' + str(fp.initial_value)
    
            if(fp.type == 'cosine, <initial_time>, <angular_vel>, <amplitude>, forever, <initial_value>'):
                fp.expression = 'cosine, ' + str(fp.initial_time.Value) + ', ' + str(fp.angular_vel) + ', ' + str(fp.amplitude) + ', forever, ' + str(fp.initial_value)
    
            if(fp.type == 'cosine, <initial_time>, <angular_vel>, <amplitude>, <number_of_cycles>, <initial_value>'):
                fp.expression = 'cosine, ' + str(fp.initial_time.Value) + ', ' + str(fp.angular_vel) + ', ' + str(fp.amplitude) + ', ' + str(fp.number_of_cycles) + ', ' + str(fp.initial_value)
    
            if(fp.type == 'cubic, <const_coef>, <linear_coef>, <parabolic_coef>, <cubic_coef>'):
                fp.expression = 'cubic, ' + str(fp.const_coef) + ', ' + str(fp.linear_coef) + ', ' + str(fp.parabolic_coef) + ', ' + str(fp.cubic_coef)
    
            if(fp.type == 'direct'):
                fp.expression = 'direct'
    
            if(fp.type == 'double ramp, <a_slope>, <a_initial_time>, <a_final_time>, <d_slope>, <d_initial_time>, forever, <initial_value>'):
                fp.expression = 'double ramp, ' + str(fp.a_slope) + ', ' + str(fp.a_initial_time.Value) + ', ' + str(fp.a_final_time.Value) + ', ' + str(fp.d_slope) + ', ' + str(fp.d_initial_time.Value) + ', forever, ' + str(fp.initial_value)
    
            if(fp.type == 'double ramp, <a_slope>, <a_initial_time>, <a_final_time>, <d_slope>, <d_initial_time>, <d_final_time>, <initial_value>'):
                fp.expression = 'double ramp, ' + str(fp.a_slope) + ', ' + str(fp.a_initial_time.Value) + ', ' + str(fp.a_final_time.Value) + ', ' + str(fp.d_slope) + ', ' + str(fp.d_initial_time.Value) + ', ' + str(fp.d_final_time.Value) + ', ' + str(fp.initial_value)
    
            if(fp.type == 'double step, <initial_time>, <final_time>, <step_value>, <initial_value>'):
                fp.expression = 'double step, ' + str(fp.initial_time.Value) + ', ' + str(fp.final_time.Value) + ', ' + str(fp.step_value) + ', ' + str(fp.initial_value)
                
            if(fp.type == 'linear, <const_coef>, <slope_coef>'):
                fp.expression = 'linear, ' + str(fp.const_coef) + ', ' + str(fp.slope_coef)
    
            if(fp.type == 'ramp, <slope>, <initial_time>, forever, <initial_value>'):
                fp.expression = 'ramp, ' + str(fp.slope) + ', ' + str(fp.initial_time.Value) + ', forever, ' + str(fp.initial_value)
    
            if(fp.type == 'ramp, <slope>, <initial_time>, <final_time>, <initial_value>'):
                fp.expression = 'ramp, ' + str(fp.slope) + ', ' + str(fp.initial_time.Value) + ', ' + str(fp.final_time.Value) + ', ' + str(fp.initial_value)
    
            if(fp.type == 'sine, <initial_time>, <angular_vel>, <amplitude>, half, <initial_value>'):
                fp.expression = 'sine, ' + str(fp.initial_time.Value) + ', ' + str(fp.angular_vel) + ', ' + str(fp.amplitude) + ', half, ' + str(fp.initial_value)
    
            if(fp.type == 'sine, <initial_time>, <angular_vel>, <amplitude>, one, <initial_value>'):
                fp.expression = 'sine, ' + str(fp.initial_time.Value) + ', ' + str(fp.angular_vel) + ', ' + str(fp.amplitude) + ', one, ' + str(fp.initial_value)
    
            if(fp.type == 'sine, <initial_time>, <angular_vel>, <amplitude>, forever, <initial_value>'):
                fp.expression = 'sine, ' + str(fp.initial_time.Value) + ', ' + str(fp.angular_vel) + ', ' + str(fp.amplitude) + ', forever, ' + str(fp.initial_value)
    
            if(fp.type == 'sine, <initial_time>, <angular_vel>, <amplitude>, <number_of_cycles>, <initial_value>'):
                fp.expression = 'sine, '+ str(fp.initial_time.Value) + ', ' + str(fp.angular_vel) + ', ' + str(fp.amplitude) + ', ' + str(fp.number_of_cycles) + ', ' + str(fp.initial_value)
            
            if(fp.type == 'step, <initial_time>, <step_value>, <initial_value>'):
                fp.expression = 'step, ' + str(fp.initial_time.Value) + ', ' + str(fp.step_value) + ', ' + str(fp.initial_value)
    
            if(fp.type == 'string'):
                fp.expression = 'string, "' + fp.string + '"'
    
            if(fp.type == 'time'):
                fp.expression = 'time'
    
            if(fp.type == 'timestep'):
                fp.expression = 'timestep'
                
            if(fp.type == 'unit'):
                fp.expression = 'unit'                 
        except:
            pass                

    def execute(self, fp):
        '''Do something when doing a recomputation, this method is mandatory'''
        FreeCAD.Console.PrintMessage("DRIVE: " +fp.label+ " successful recomputation...\n")
                              
    def onDocumentRestored(self, obj):
        obj.a_initial_time = FreeCAD.Units.Unit('s')
        obj.d_initial_time = FreeCAD.Units.Unit('s')
        obj.a_final_time = FreeCAD.Units.Unit('s')
        obj.d_final_time = FreeCAD.Units.Unit('s')
        obj.initial_time = FreeCAD.Units.Unit('s')
        obj.final_time = FreeCAD.Units.Unit('s')