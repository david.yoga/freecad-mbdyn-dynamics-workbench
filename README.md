Please be aware that this software is in an early stage of development, 
although it is becoming usable at a steady phase. 

Tutorials coming soon...
 
If you find this project interesting and you like to program in Python and/or C++, 
please consider helping with its development.
If you would like to contribute, please contact me at josegegas@gmail.com

You can see some application examples in my Youtube channel:

https://www.youtube.com/user/josegegas/videos

These examples are not meant to be "good MBD" simulations. 
Rather, they are meant to be good tests for this software. 
What I test is the behaviour of the "FreeCAD-MBDyn dynamics workbench". 

You can downoad these examples from the "Examples" folder.

Open an example and swithc to the "Dynamics" workbench. Click on "Write input file". The workbench will automatically generate an input file for MBDyn, using information from the CAD model and some FreeCAD "scripted objects" representing joints, forces, couples, drives, scalar functions, etc. 

If you wish to learn more about scripted objects you can visit:

https://wiki.freecadweb.org/Scripted_objects

If you wish to learn more about input files for MBDyn you can visit:

https://www.sky-engin.jp/en/MBDynTutorial/

Another basic tool to learn how to write input files is the input file format:

https://kitefast.readthedocs.io/en/latest/_downloads/f95b077fcef9a3ee4891f17a047c1252/mbdyn-input-1.7.3.pdf

Please use this workbench with FreeCAD 0.19 or higher.

This workbench has been tested on Windows 7 and Ubuntu 20.04. If you test it in another system, please let me know.

Please follow the next steps to get the software running on Unbuntu:

1-      Download and Install any Ubuntu Base.
2-      After Installing, run `apt-get update`
3-      Install the latest updates for gfortran and C++.
4-      Download the MBDyn latest release from. https://www.mbdyn.org/?Software_Download
5-      Untar the downloaded file.
6-      In the directory of mbdyn[$version], configure the source code by writing the command `./configure --prefix=/usr/local`
7-      Make it by writing this command In the directory of mbdyn[$version] `make`
8-      Install it by writing this command In the directory of mbdyn[$version] `sudo make install`
9-      Download and install FreeCAD 0.19
10-      Download the source code of the workbench, extract it and paste it into your Mod folder.
11-      Start FreeCAD. The new workbench will be available.

NOTE:

You may need to install mpmath and sympy. Find the instructions to do so next. 
The isntalation process is the same in both, Windows and Ubuntu.

Please follow the next steps to get the software running on Windows:

1.- Download FreeCAD_weekly-builds-24276-Win-Conda_vc14.x-x86_64.7z from here: https://github.com/FreeCAD/FreeCAD-AppI ... kly-builds
2.- Extract FreeCAD to any location on your PC.
3.- Download mpmath-1.2.0.tar from here: https://mpmath.org/
4.- Extract mpmath-1.2.0.tar to any location on your PC.
5.- Download sympy-1.7.1.tar from here: https://github.com/sympy/sympy/releases
6.- Extract sympy-1.7.1.tar to any location on your PC.
7.- In the extracted folder "mpmath-1.2.0" you will find a folder named "mpmath". Copy and paste it in the "bin" folder, which is in "FreeCAD_weekly-builds-24276-Win-Conda_vc14.x-x86_64" folder you extracted before.
8.- In the extracted folder "sympy-1.7.1" you will find a folder named "sympy". Copy and paste it in the "bin" folder, which is in "FreeCAD_weekly-builds-24276-Win-Conda_vc14.x-x86_64" folder you extracted before.
9.- Download the "FreeCAD-MBDyn" workbench from here: https://gitlab.com/josegegas/freecad-mb ... -workbench
10.- Extract the workbench and paste the whole folder ("freecad-mbdyn-dynamics-workbench-master") into the "Mod" folder, which is in "FreeCAD_weekly-builds-24276-Win-Conda_vc14.x-x86_64".
11.- Execute FreeCAD. The executable file is in the "bin" folder within "FreeCAD_weekly-builds-24276-Win-Conda_vc14.x-x86_64", it is "freecad.exe".
12.- That is it, the workbench should now work and you should be able to load the examples I have included with it. 

Enjoy!!
