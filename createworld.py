# -*- coding: utf-8 -*-
###################################################################################
#
#  Copyright 2021 Jose Gabriel Egas Ortuno
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#
###################################################################################

import FreeCAD
import FreeCADGui
import Draft

from customviews import PrismaticCustomView, StaticBodyCustomView, RigidBodyCustomView, GearCustomView, RevoluteRotationCustomView, TotalCustomView, Beam3CustomView
from customviews import DummyBodyCustomView, RevolutePinCustomView, RevoluteHingeCustomView, ClampCustomView, InLineCustomView, AxialCustomView, LockCustomView
from customviews import StructuralCoupleCustomView, StructuralForceCustomView, DriveHingeCustomView, SphericalCustomView, DeformableDisplacementCustomView, InPlaneCustomView
from customviews import CylindricalCustomView, SliderCustomView, FlexibleBodyCustomView, ViscousBodyCustomView, TotalPinCustomView, GenelClampCustomView

class Createworld: 
    def __init__(self):
        l = []            
        for obj in FreeCAD.ActiveDocument.Objects:
            try:
                if abs(obj.Shape.BoundBox.XMax) < 1000000 :
                    l.append(abs(obj.Shape.BoundBox.XMax))
                    l.append(abs(obj.Shape.BoundBox.XMin))
                    l.append(abs(obj.Shape.BoundBox.YMax))
                    l.append(abs(obj.Shape.BoundBox.YMin))
                    l.append(abs(obj.Shape.BoundBox.ZMax))
                    l.append(abs(obj.Shape.BoundBox.ZMin))                
            except:
                pass
                      
        Length = max(l)*4.0       
        p1 = FreeCAD.Vector(-Length/2, 0, 0)
        p2 = FreeCAD.Vector(Length/2, 0, 0)
        Draft.makeLine(p1, p2)
        FreeCAD.ActiveDocument.getObject("Line").Label = "X"
        FreeCADGui.ActiveDocument.getObject("Line").LineColor = (1.00,0.00,0.00)
        FreeCADGui.ActiveDocument.getObject("Line").PointColor = (1.00,0.00,0.00)
        FreeCADGui.ActiveDocument.getObject("Line").DrawStyle = u"Dotted"
        FreeCADGui.ActiveDocument.getObject("Line").PointSize = 1.00
        FreeCADGui.ActiveDocument.getObject("Line").LineWidth = 1.00
        FreeCADGui.ActiveDocument.getObject("Line").Selectable = False

        p1 = FreeCAD.Vector(0, -Length/2, 0)
        p2 = FreeCAD.Vector(0, Length/2, 0)
        Draft.makeLine(p1, p2)
        FreeCAD.ActiveDocument.getObject("Line001").Label = "Y"
        FreeCADGui.ActiveDocument.getObject("Line001").LineColor = (0.00,1.00,0.00)
        FreeCADGui.ActiveDocument.getObject("Line001").PointColor = (0.00,1.00,0.00)
        FreeCADGui.ActiveDocument.getObject("Line001").DrawStyle = u"Dotted"
        FreeCADGui.ActiveDocument.getObject("Line001").PointSize = 1.00
        FreeCADGui.ActiveDocument.getObject("Line001").LineWidth = 1.00
        FreeCADGui.ActiveDocument.getObject("Line001").Selectable = False

        p1 = FreeCAD.Vector(0, 0, -Length/2)
        p2 = FreeCAD.Vector(0, 0, Length/2)
        Draft.makeLine(p1, p2)
        FreeCAD.ActiveDocument.getObject("Line002").Label = "Z"
        FreeCADGui.ActiveDocument.getObject("Line002").LineColor = (0.00,0.00,1.00)
        FreeCADGui.ActiveDocument.getObject("Line002").PointColor = (0.00,0.00,1.00)
        FreeCADGui.ActiveDocument.getObject("Line002").DrawStyle = u"Dotted"
        FreeCADGui.ActiveDocument.getObject("Line002").PointSize = 1.00
        FreeCADGui.ActiveDocument.getObject("Line002").LineWidth = 1.00
        FreeCADGui.ActiveDocument.getObject("Line002").Selectable = False
        
        FreeCADGui.SendMsgToActiveView("ViewFit")

        FreeCADGui.activeDocument().activeView().viewAxonometric()
        #Add containers:
        FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroup","Bodies_and_assemblies")
        #Move all the solid objects in the "Solid_objects" group:
        #for obj in FreeCAD.ActiveDocument.Objects[:-1]:
        #    if str(type(obj)) == "<class 'Part.Feature'>" or str(type(obj)) == "<class 'PartDesign.Body'>":
        #           FreeCAD.ActiveDocument.getObject("Solid_objects").addObject(obj)    
        
        for obj in FreeCAD.ActiveDocument.RootObjects[:-1]:        
            FreeCAD.ActiveDocument.getObject("Bodies_and_assemblies").addObject(obj)  
            
        
        elements = FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroup","MBDyn_elements")  
        
        gravity_elements = FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroup","Gravity_elements")
        elements.addObject(gravity_elements)
        
        body_elements_and_nodes = FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroup","Body_elements_and_nodes")
        elements.addObject(body_elements_and_nodes)
        
        rigid_body_elements = FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroupPython","Rigid_body_elements")
        body_elements_and_nodes.addObject(rigid_body_elements) 
        RigidBodyCustomView(rigid_body_elements.ViewObject)
        
        beam_elements = FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroupPython","Beam_elements")
        body_elements_and_nodes.addObject(beam_elements) 
        Beam3CustomView(beam_elements.ViewObject)
        
        generic = FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroupPython","Generic")
        rigid_body_elements.addObject(generic) 
        
        gears = FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroupPython","Gears")
        rigid_body_elements.addObject(gears) 
        GearCustomView(gears.ViewObject)
        
        dummy_body_elements = FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroupPython","Dummy_body_elements")
        body_elements_and_nodes.addObject(dummy_body_elements)
        DummyBodyCustomView(dummy_body_elements.ViewObject)
        
        static_body_elements = FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroupPython","Static_body_elements")
        body_elements_and_nodes.addObject(static_body_elements) 
        StaticBodyCustomView(static_body_elements.ViewObject)
        
        ################################################JOINTS:######################################
        joint_elements = FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroup","Joint_elements")
        elements.addObject(joint_elements)
                
        obj = FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroupPython","Revolute_pin_joints")
        joint_elements.addObject(obj) 
        RevolutePinCustomView(obj.ViewObject)
        
        obj = FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroupPython","Revolute_hinge_joints")
        joint_elements.addObject(obj) 
        RevoluteHingeCustomView(obj.ViewObject)
        
        obj = FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroupPython","Clamp_joints")
        joint_elements.addObject(obj) 
        ClampCustomView(obj.ViewObject)
        
        obj = FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroupPython","Spherical_hinge_joints")
        joint_elements.addObject(obj) 
        SphericalCustomView(obj.ViewObject)
        
        obj = FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroupPython","Viscous_body_joints")
        joint_elements.addObject(obj) 
        ViscousBodyCustomView(obj.ViewObject)
        
        obj = FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroupPython","In_line_joints")
        joint_elements.addObject(obj) 
        InLineCustomView(obj.ViewObject)
        
        obj = FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroupPython","In_plane_joints")
        joint_elements.addObject(obj) 
        InPlaneCustomView(obj.ViewObject)
        
        obj = FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroupPython","Total_joints")
        joint_elements.addObject(obj) 
        TotalCustomView(obj.ViewObject)

        obj = FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroupPython","Total_pin_joints")
        joint_elements.addObject(obj) 
        TotalPinCustomView(obj.ViewObject)

        obj = FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroupPython","Revolute_rotation_joints")
        joint_elements.addObject(obj) 
        RevoluteRotationCustomView(obj.ViewObject)
        
        obj = FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroupPython","Axial_rotation_joints")
        joint_elements.addObject(obj) 
        AxialCustomView(obj.ViewObject)

        obj = FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroupPython","Drive_hinge_joints")
        joint_elements.addObject(obj) 
        DriveHingeCustomView(obj.ViewObject)
        
        obj = FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroupPython","Prismatic_joints")
        joint_elements.addObject(obj) 
        PrismaticCustomView(obj.ViewObject)

        obj = FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroupPython","Deformable_displacement_joints")
        joint_elements.addObject(obj) 
        DeformableDisplacementCustomView(obj.ViewObject)

        ############################################################ COMPOUND JOINTS ##############################################
        compound_joint_elements = FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroup","Compound_joint_elements") 
        elements.addObject(compound_joint_elements)

        obj = FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroupPython","Coincidence_joints")
        compound_joint_elements.addObject(obj)         
        
        obj = FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroupPython","Cylindrical_joints")
        compound_joint_elements.addObject(obj)         
        
        obj = FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroupPython","Slider_joints")
        compound_joint_elements.addObject(obj)         

        #################################################################### FORCES AND COUPLES ###################################
        structural_forces_elements = FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroup","Structural_force_elements")
        elements.addObject(structural_forces_elements)
        
        structural_couple_elements = FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroup","Structural_couple_elements")
        elements.addObject(structural_couple_elements)
        
        genel_elements = FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroup","Genel_elements")
        elements.addObject(genel_elements)
        
        genel_clamps = FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroupPython","Genel_clamps")
        genel_elements.addObject(genel_clamps) 
        GenelClampCustomView(genel_clamps.ViewObject)
        
        FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroup","Abstract_nodes") 
        FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroup","Drives and drive callers")         
        FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroup","Scalar_functions")    
        FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroup","Constitutive_laws")
        FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroup","MBDyn_simulation")
        FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroup","Global_reference_frame")
        FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroup","Input_files")
        FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroup","User_defined_variables")
        
        #Add the input files:
        obj = FreeCAD.ActiveDocument.addObject("App::TextDocument", "input_file")
        FreeCAD.ActiveDocument.getObject("Input_files").addObject(obj)
        obj = FreeCAD.ActiveDocument.addObject("App::TextDocument", "input_file_aux")
        FreeCAD.ActiveDocument.getObject("Input_files").addObject(obj)
        obj = FreeCAD.ActiveDocument.addObject("App::TextDocument", "variables")
        FreeCAD.ActiveDocument.getObject("User_defined_variables").addObject(obj)
        
        #Move axes into their container:
        FreeCAD.ActiveDocument.getObject("Global_reference_frame").addObject(FreeCAD.ActiveDocument.getObject("Line"))
        FreeCAD.ActiveDocument.getObject("Global_reference_frame").addObject(FreeCAD.ActiveDocument.getObject("Line001"))
        FreeCAD.ActiveDocument.getObject("Global_reference_frame").addObject(FreeCAD.ActiveDocument.getObject("Line002"))
        #Add lines to mark center of mass of any body:
        FreeCAD.ActiveDocument.addObject("Part::Line","cmx")
        FreeCAD.ActiveDocument.addObject("Part::Line","cmy")
        FreeCAD.ActiveDocument.addObject("Part::Line","cmz")   
        FreeCADGui.ActiveDocument.getObject("cmx").LineWidth = 1.00
        FreeCADGui.ActiveDocument.getObject("cmy").LineWidth = 1.00
        FreeCADGui.ActiveDocument.getObject("cmz").LineWidth = 1.00
        FreeCADGui.ActiveDocument.getObject("cmx").PointSize = 1.00
        FreeCADGui.ActiveDocument.getObject("cmy").PointSize = 1.00
        FreeCADGui.ActiveDocument.getObject("cmz").PointSize = 1.00
        FreeCADGui.ActiveDocument.getObject("cmx").DrawStyle = u"Dotted"
        FreeCADGui.ActiveDocument.getObject("cmy").DrawStyle = u"Dotted"
        FreeCADGui.ActiveDocument.getObject("cmz").DrawStyle = u"Dotted" 
        #Move the lines to their folder:        
        FreeCAD.ActiveDocument.getObject("Global_reference_frame").addObject(FreeCAD.ActiveDocument.getObject("cmx"))
        FreeCAD.ActiveDocument.getObject("Global_reference_frame").addObject(FreeCAD.ActiveDocument.getObject("cmy"))
        FreeCAD.ActiveDocument.getObject("Global_reference_frame").addObject(FreeCAD.ActiveDocument.getObject("cmz"))
        
        
        FreeCAD.ActiveDocument.recompute()
        """
        



        obj = FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroupPython","Flexible_bodies")
        FreeCAD.ActiveDocument.getObject("Bodies").addObject(obj) 
        FlexibleBodyCustomView(obj.ViewObject)
        
        obj = FreeCAD.ActiveDocument.addObject("App::DocumentObjectGroupPython","Beam3")
        FreeCAD.ActiveDocument.getObject("Flexible_bodies").addObject(obj)
                    

"""
        
        